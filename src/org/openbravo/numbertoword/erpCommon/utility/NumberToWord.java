/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SL 
 * All portions are Copyright (C) 2013-2016 Openbravo SL 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.numbertoword.erpCommon.utility;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;

public abstract class NumberToWord {
  public static String getNumberToWord(
      org.openbravo.model.common.enterprise.Organization organization) throws Exception {
    try {
      String javaclassname = null;
      OBContext.setAdminMode(true);
      OBCriteria<org.openbravo.numbertoword.NumberToWord> ntw = OBDal.getInstance().createCriteria(
          org.openbravo.numbertoword.NumberToWord.class);
      ntw.add(Restrictions.eq(org.openbravo.numbertoword.NumberToWord.PROPERTY_ORGANIZATION,
          organization));
      ntw.setFilterOnActive(true); // Only active
      ntw.setFilterOnReadableClients(false);
      ntw.setFilterOnReadableOrganization(false);

      List<org.openbravo.numbertoword.NumberToWord> ntwList = ntw.list();

      if (ntwList.size() == 0) {
        javaclassname = null;
      } else if (ntwList.size() > 1) {

        throw new OBException(OBMessageUtils.messageBD("obntw_ErrorConvert"));
      } else {
        javaclassname = ntwList.get(0).getJavaclass();
      }

      return javaclassname;

    } finally {
      OBContext.restorePreviousMode();
    }

  }

  public abstract String convert(BigDecimal number);

}
